﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinFormForWindows10_4KDPIScalingTest.Core.Application;
using WinFormForWindows10_4KDPIScalingTest.Core.Options;

namespace WinFormForWindows10_4KDPIScalingTest.Core
{
    public enum State
    {
        Idle,
        Starting,
        Loading,
        Running,
        Exiting,
        Closed,
    }
    public class WinFormApplication : IWinFormApplication
    {
        private AppServiceProvider _serviceProvider;
        private State _state = State.Starting;
        private AppPreferences _appPreferences;

        private void ThrowIfClosed()
        {
            if (this._state == State.Closed)
                throw new ObjectDisposedException(this.GetType().Name);
        }
        public AppServiceProvider ServiceProvider
        {
            get
            {
                this.ThrowIfClosed();
                return this._serviceProvider;
            }
        }
        public AppPreferences AppPreferences
        {
            get
            {
                this.ThrowIfClosed();
                return this._appPreferences;
            }
        }
    }
}
