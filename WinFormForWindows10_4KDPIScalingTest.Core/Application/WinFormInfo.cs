﻿using System;
using System.Reflection;
using System.IO;

namespace WinFormForWindows10_4KDPIScalingTest.Core
{
    public static class WinFormInfo
    {
        private const string ApplicationSubDirectory = @"Keysight\Fusion";
        public static readonly string DefaultDocumentDir;
        public static readonly string ApplicationDataDir;
        public static readonly string AppPath;
        public static readonly string SharedAssemblyDir;
        static WinFormInfo()
        {
            // Common Info Settings
            Assembly executingAssembly = Assembly.GetExecutingAssembly();

            // Application Path
            if (executingAssembly.Location.Contains("SharedAssemblies"))
            {
                SharedAssemblyDir = Path.GetDirectoryName(executingAssembly.Location);
                AppPath = Path.GetDirectoryName(SharedAssemblyDir);
            }
            else
            {
                SharedAssemblyDir = Path.GetDirectoryName(executingAssembly.Location);
                AppPath = SharedAssemblyDir;
            }
            // Appliation Data Directory
            string appDataDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            appDataDir = Path.Combine(appDataDir, ApplicationSubDirectory); // Add Agilent/Fusion
            if (!Directory.Exists(appDataDir))
            {
                try
                {
                    Directory.CreateDirectory(appDataDir);
                }
                catch (Exception)
                {
                    appDataDir = Environment.CurrentDirectory;
                }
            }
            ApplicationDataDir = appDataDir;
        }
    }
}
