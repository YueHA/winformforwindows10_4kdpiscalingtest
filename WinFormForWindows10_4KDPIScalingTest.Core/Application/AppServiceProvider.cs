﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormForWindows10_4KDPIScalingTest.Core
{
    sealed public class AppServiceProvider:IWinFormServiceProvider
    {
        public object GetService(Type serviceType)
        {
            if (this._serviceMap.ContainsKey(serviceType))
            {
                return this._serviceMap[serviceType];
            }

            return null;
        }
        private readonly Dictionary<Type, object> _serviceMap = new Dictionary<Type, object>();
        public T GetService<T>()
        {
            return (T)this.GetService(typeof(T));
        }
        public void AddService<T>(T instance)
        {
            this._serviceMap[typeof(T)] = instance;
        }
    }
}
