﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormForWindows10_4KDPIScalingTest.Core
{
    public interface IWinFormServiceProvider: IServiceProvider
    {
        T GetService<T>();
    }
}
