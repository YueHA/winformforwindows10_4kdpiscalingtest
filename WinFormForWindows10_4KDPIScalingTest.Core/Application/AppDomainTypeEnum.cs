﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormForWindows10_4KDPIScalingTest.Core.Application
{
    public enum AppDomainTypeEnum
    {
        DefaultAppDomain,
        AllTestsInSingleAppDomain,
    }
}
