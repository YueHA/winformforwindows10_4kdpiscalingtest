﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using WinFormForWindows10_4KDPIScalingTest.Core;

namespace WinFormForWindows10_4KDPIScalingTest.Core.Options
{
    [Serializable]
    public class AppPreferences
    {
        [XmlIgnore]
        private bool _scenarioOwnsPrefs;

        // Preference application storage data
        private readonly Dictionary<string, object> _internalData = new Dictionary<string, object>();
        private readonly Dictionary<string, object> _prefData = new Dictionary<string, object>();
        private readonly Dictionary<string, object> _origPrefData = new Dictionary<string, object>();

        /// <summary>
        /// Default Constructor
        /// </summary>
        public AppPreferences()
        {
            // Set defaults
            this._internalData["OpenPreviousScenario"] = false;
            this._internalData["PreviousScenarioFile"] = "";
            this._internalData["ClearLogOnRunStart"] = false;
            this._internalData["STAState"] = true;
            //this._internalData["ParameterHistory"] = new XmlDictionary<string, string[]>();
            this._internalData["ShowRunSummary"] = false;

#if !WindowsCE
            this._internalData["RunTestsInTestAppDomain"] = true;
#endif
            this._internalData["DefaultScenarioFile"] = Path.Combine(WinFormInfo.AppPath, @"Templates\DefaultScenario.fsxml");
            this._internalData["TemplateVariableFile"] = String.Empty;
            this._internalData["DefaultVariableFile"] = Path.Combine(WinFormInfo.AppPath, @"Templates\DefaultVariables.fvxml");
            this._internalData["DefaultVariableDir"] = WinFormInfo.DefaultDocumentDir;
            this._internalData["CheckVariableUsage"] = false;
        }

    }
}