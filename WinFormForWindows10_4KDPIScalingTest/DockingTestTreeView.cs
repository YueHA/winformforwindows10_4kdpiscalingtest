﻿using System;

using WinFormForWindows10_4KDPIScalingTest.UI.TestTree;

using WeifenLuo.WinFormsUI.Docking;

namespace WinFormForWindows10_4KDPIScalingTest.GraphicalApp
{
    partial class DockingTestTreeView : DockContent
    {
        private WinFormForWindows10_4KDPIScalingTest.UI.TestTree.TestTreeUC TestTreeUC1;
        private string _titleRoot;
        private string _selectedNode;
        public DockingTestTreeView()
        {
            this._titleRoot = "Test Tree: 0/0";
            this.InitializeComponent();
        }
        public WinFormForWindows10_4KDPIScalingTest.UI.TestTree.TestTreeUC TestTreeUC
        {
            get { return this.TestTreeUC1; }
        }
        private void InitializeComponent()
        {
            this.testTreeUC2 = new WinFormForWindows10_4KDPIScalingTest.UI.TestTree.TestTreeUC();
            this.SuspendLayout();
            // 
            // testTreeUC2
            // 
            this.testTreeUC2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.testTreeUC2.Location = new System.Drawing.Point(0, 0);
            this.testTreeUC2.Margin = new System.Windows.Forms.Padding(4);
            this.testTreeUC2.Name = "testTreeUC2";
            this.testTreeUC2.Size = new System.Drawing.Size(843, 476);
            this.testTreeUC2.TabIndex = 0;
            // 
            // DockingTestTreeView
            // 
            this.ClientSize = new System.Drawing.Size(843, 476);
            this.CloseButton = false;
            this.Controls.Add(this.testTreeUC2);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HideOnClose = true;
            this.Name = "DockingTestTreeView";
            this.TabText = "Test Tree: 0/0";
            this.Load += new System.EventHandler(this.DockingTestTreeView_Load);
            this.ResumeLayout(false);

        }
        /*private void TestTreeUC1_Load(object sender, EventArgs e)
        {
            
        }*/
        private void DockingTestTreeView_Load(object sender, EventArgs e)
        {

        }

    }
}
