﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinFormForWindows10_4KDPIScalingTest.Core;
using WinFormForWindows10_4KDPIScalingTest.UI;
using WeifenLuo.WinFormsUI.Docking;


namespace WinFormForWindows10_4KDPIScalingTest.GraphicalApp
{
    public partial class Form1 : Form
    {
        private static readonly string DockConfigFile = Path.Combine(WinFormInfo.ApplicationDataDir, "FusionDockingLayout.3.0.config");
        private WinFormApplication App = null;

        DockingTestTreeView testTreeView = null;
        DeserializeDockContent deserializeDockContent;
        public Form1()
        {
            InitializeComponent();

            this.testTreeView = new DockingTestTreeView();

            // Subscribe to test tree events
            //this.testTreeView.TestTreeUC.InitializeServices(this.App.ServiceProvider);
            /*this.testTreeView.TestTreeUC.TestSelected += new EventHandler<TestTreeEventArgs>(TestTreeUC_TestSelected);
            this.testTreeView.TestTreeUC.AddTestCommand += new EventHandler<TestTreeEventArgs>(TestTreeUC_AddTestCommand);
            this.testTreeView.TestTreeUC.ViewSource += new EventHandler<TestTreeEventArgs>(TestTreeUC_ViewSource);
            this.testTreeView.TestTreeUC.ViewInformation += new EventHandler<TestTreeEventArgs>(TestTreeUC_ViewInformation);*/
            //this.App.ServiceProvider.AddService<WinFormForWindows10_4KDPIScalingTest.UI.TestTree.TestTreeUC>(this.testTreeView.TestTreeUC);
                        
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            bool success = false;
            

            if (!success)
            {
                // Layout manually...
                this.dockPanel1.SuspendLayout(true);

                //this.SetDockingLayout("HideAll");
                this.SetDockingLayout("Reset");

                this.dockPanel1.ResumeLayout(true, true);
            }

            if (!this.testTreeView.IsActivated)
            {
                this.testTreeView.Show(this.dockPanel1, DockState.DockTop);
                this.testTreeView.Activate();
            }
            // Set Dock State
            bool lockState=false; //= this.App.AppPreferences.GetInternal<bool>("GUI.DockingPanel.Locked", false);
            //this.ToolViewLockToolStripMenuItem.Checked = lockState;
            //this.ToolViewLockToolStripMenuItem.Tag = lockState ? "Unlock" : "Lock";
            this.dockPanel1.AllowEndUserDocking = !lockState;
        }
        private void SetDockingLayout(string layout)
        {
            switch (layout)
            {
                case "Lock":
                    this.dockPanel1.AllowEndUserDocking = false;
                    
                    break;

                case "Unlock":
                    this.dockPanel1.AllowEndUserDocking = true;

                    break;

                case "HideAll":
                    
                    this.testTreeView.Hide();
                   
                    break;

                case "ShowAll":
                    // Top Panel
                    if (this.testTreeView.IsHidden) this.testTreeView.Show(this.dockPanel1, DockState.DockTop);
                    break;

                case "Fixed":
                    this.SetDockingLayout("HideAll");

                    //this.logWindow.Show(this.dockPanel1);
                    this.testTreeView.Show(this.dockPanel1, DockState.DockTop);
                   
                    this.SetDockingLayout("Lock");
                    break;

                case "Reset":
                    this.SetDockingLayout("ShowAll");
                    this.SetDockingLayout("Unlock");

                    break;
            }
        }
        /*private void TestTreeUC_ViewInformation(object sender, TestTreeEventArgs e)
        {
            // Check to see the selected node is a test instance
            TestInstance test = e.TestObject as TestInstance;
            TestCollection collection = e.TestObject as TestCollection;

            if (test != null)
                this.TestTreeUC_ViewInstanceInformation(test);

            else if (collection != null)
                this.TestTreeUC_ViewCollectionInformation(collection);
        }*/
        private IDockContent GetContentFromPersistString(string persistString)
        {          
            if (persistString == typeof(DockingTestTreeView).ToString() || persistString == "WinFormForWindows10_4KDPIScalingTest.GraphicalApp.DockingTestTree")
                return this.testTreeView;
            else
                return null;
        }
        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void toolStripLabel2_Click(object sender, EventArgs e)
        {

        }

        private void toolStripLabel3_Click(object sender, EventArgs e)
        {

        }
        private void dockPanel1_ActiveContentChanged(object sender, EventArgs e)
        {

        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {

        }

        private void toolStripContainer1_TopToolStripPanel_Click(object sender, EventArgs e)
        {

        }

        private void toolStripDropDownButton3_Click(object sender, EventArgs e)
        {

        }
    }
}
