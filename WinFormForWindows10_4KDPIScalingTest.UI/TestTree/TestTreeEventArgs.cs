﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

using WinFormForWindows10_4KDPIScalingTest.Core;


namespace WinFormForWindows10_4KDPIScalingTest.UI
{
    public class TestTreeEventArgs : EventArgs
    {
        private TreeNode _node;
        private List<string> _testFiles;
        private int _loaded;
        private int _selected;

        public TestTreeEventArgs()
            : base()
        {
        }   

        public int LoadedCount
        {
            get { return this._loaded; }
            set { this._loaded = value; }
        }

        public int SelectedCount
        {
            get { return this._selected; }
            set { this._selected = value; }
        }
        public TreeNode TreeNode
        {
            get { return this._node; }
            set { this._node = value; }
        }

        public List<string> TestFiles
        {
            get { return this._testFiles; }
            set { this._testFiles = value; }
        }
    }
}
